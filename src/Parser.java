import java.util.HashMap;
import java.util.Map;

class Parser {
    private static Map<String, Type> _varibles = new HashMap<String, Type>();
    private static final int QOUTE_LEN = 1;
    private static boolean isString(String str){
        if (str.charAt(0) == "'".charAt(0) &&
                str.charAt(str.length() - 1) == "'".charAt(0)) {
            for(int i = QOUTE_LEN; i < str.length() - QOUTE_LEN; i++){
                if (str.charAt(i) == "'".charAt(0)){
                    return false;
                }
            }
            return true;
        }
        else if (str.charAt(0) == '"'&&
                str.charAt(str.length() - 1) == '"'){
            for(int i = QOUTE_LEN; i < str.length() - QOUTE_LEN; i++) {
                if (str.charAt(i) == '"') {
                    return false;
                }
            }
            return true;
            }
        return false;
    }

    private static boolean isOnlyOneEqual(String str){
        int counter = 0;
        for (int i =0; i < str.length(); i++){
            if(str.charAt(i) == '='){
                counter++;
            }
        }
        return counter == 1;
    }
    private static boolean isVariableLegal(String name){
        if (name.length() == 0){
            return false;
        }
        else if (name.charAt(0)<'9' && name.charAt(0)> '0' ){
            return false;
        }
        for(int i =0; i < name.length();i++){
            if (!(
                    (name.charAt(i)<='9' && name.charAt(i)>= '0')||
                            name.charAt(i) == '_'||
                            (name.charAt(i)>='a'&& name.charAt(i)<='z')
                    )){
                return false;
            }
        }
        return true;
    }
    static Type parseString(String str) throws SyntaxException, NameErrorException, IndentationException{
        if (str.charAt(0) == ' '){
            throw new IndentationException();
        }
        str = Utilities.removeBegEndSpaces(str);
        Type value;
        if(null != (value = getVariableValue(str))){
            return value;
        }
        else if (null != (value = getType(str))){
            return value;
        }
        else if (makeAssignment(str)) {
            return new Void(false);
        }
        throw new SyntaxException();
    }
    private static List getList(String str) throws SyntaxException{
        List aList;
        Type listElenemt;
        String[] elements;
        if(0 == str.length()){
            return null;
        }
        if(str.charAt(0) != '[' || str.charAt(str.length() - 1) != ']') {
            return null;
        }
        for(int i =1; i < str.length() - 1; i++){
            if (str.charAt(i) == '[' || str.charAt(i) == ']'){
                return null;
            }
        }
        str = (str.split("\\[")[1]).split("]")[0];
        elements = str.split(",");
        aList = new List();
        for (String element:elements) {
            listElenemt= getType(Utilities.removeBegEndSpaces(element));
            if(null == listElenemt ){
                throw new SyntaxException();
            }
            else{
                aList.append(listElenemt);
            }
        }
        return aList;
    }
    private static Type getType(String str) throws SyntaxException{
        StringBuilder strBuilder;
        Type variableObj;
        if(null != (variableObj = getList(str))){
            return variableObj;
        }
        else if (str.equals("True")){
            return new Bool(true, true);
        }
        else if(str.equals("False")){
            return new Bool(false, true);
        }
        else if(isString(str)){
            strBuilder = new StringBuilder();
            for (int i = QOUTE_LEN; i < str.length() - QOUTE_LEN; i++){
                strBuilder.append(str.charAt(i));
            }
            return new MyString(strBuilder.toString(), true);
        }
        try{
            return new Int(Integer.parseInt(str), true);
        }
        catch(NumberFormatException exc){
            return null;
        }
    }
    private static boolean makeAssignment(String str)throws SyntaxException, NameErrorException{
        String name;
        String value;
        String[] arr;
        boolean isNameLegal;
        Type valueObj;
        Type tempValueObj;
        if(!isOnlyOneEqual(str)){
            return false;
        }
        arr = str.split("=");
        if (arr.length != 2){
            return false;
        }
        name = Utilities.removeBegEndSpaces(arr[0]);
        value = Utilities.removeBegEndSpaces(arr[1]);
        if(! isVariableLegal(name)){
            throw new SyntaxException();
        }
        valueObj = getType(value);
        if(valueObj != null ){
        }
        else if ((isNameLegal = isVariableLegal(name)) && _varibles.containsKey(value)){
            tempValueObj = _varibles.get(value);
            if ("Int".equals(tempValueObj.getType())){
                valueObj = new Int((Int)tempValueObj);
            }
            else if ("Bool".equals(tempValueObj.getType())){
                valueObj = new Bool((Bool)tempValueObj);
            }
            else if ("MyString".equals(tempValueObj.getType())){
                valueObj = new MyString((MyString)tempValueObj);
            }
            else{//is a List
                valueObj = tempValueObj;
            }
            valueObj.set_isTemp(false);
        }
        else if(isNameLegal){
            throw new NameErrorException(value);
        }
        else {
            throw new SyntaxException();
        }
        _varibles.put(name, valueObj);
        return true;
    }

    private static Type getVariableValue(String str) {
        if (!_varibles.containsKey(str)) {
            return null;
        } else {
            return _varibles.get(str);
        }
    }
}
