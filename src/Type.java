abstract class Type {
    private boolean _isTemp = false;

    abstract boolean isPrintable();

    abstract String getType();

    public abstract String toString();

    boolean get_isTemp(){
        return _isTemp;
    }

    void set_isTemp(boolean value){
        _isTemp = value;
    }
}
