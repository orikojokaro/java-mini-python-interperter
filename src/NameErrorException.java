public class NameErrorException extends InterperterException {
    private String _name;
    NameErrorException(String name){
        _name = name;
    }
    public String what(){
        return new String("NameError : name ") + _name + "is not defined";
    }
}
