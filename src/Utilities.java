public class Utilities {
    static String removeBegEndSpaces(String str){
        StringBuilder strBuilder = new StringBuilder();
        int i = 0;
        int strLen = str.length();
        for (; i < strLen; i++) {
            if (str.charAt(i) != ' '){
                break;
            }
        }
        for(;i < strLen;i++){
            strBuilder.append(str.charAt(i));
        }
        for(i = strLen - 1; i >= 0; i--){
            if (str.charAt(i) != ' '){
                break;
            }
        }
        i = i - (strLen - strBuilder.length());
        i++;
        strBuilder.delete(i, strBuilder.length());
        return strBuilder.toString();
    }
}
