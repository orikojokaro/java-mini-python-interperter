public class Void extends Type {
    Void(){

    }
    Void(boolean isTemp){
        set_isTemp(isTemp);
    }
    @Override
    boolean isPrintable(){
        return false;
    }
    @Override
    public String toString(){//TODO return something else?
        return "";
    }
    @Override
    String getType(){
        return "Void";
    }

}
