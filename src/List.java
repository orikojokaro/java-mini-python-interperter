import java.util.LinkedList;

public class List extends Sequence {
    private LinkedList<Type> _elements = new LinkedList<>();

    @Override
    public String toString(){
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append('[');
        for (Type element:_elements) {
            strBuilder.append((element.toString() + ", "));
        }
        if(strBuilder.charAt(strBuilder.length() - 1) == ' '){
            strBuilder.delete(strBuilder.length() - 2, strBuilder.length());
        }
        strBuilder.append(']');
        return strBuilder.toString();
    }
    void append(Type toAppend){
        _elements.add(toAppend);
    }

    @Override
    boolean isPrintable() {
        return true;
    }

    @Override
    String getType() {
        return "List";
    }
}
