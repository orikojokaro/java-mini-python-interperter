class Int extends Type {
    private int _value;
    Int(int value){
        _value = value;
    }

    Int(int value, boolean isTemp){
        _value = value;
        set_isTemp(isTemp);
    }

    Int(Int value){
        _value = value._value;
        set_isTemp(value.get_isTemp());
    }
    @Override
    String getType(){
        return "Int";
    }

    @Override
    boolean isPrintable(){
        return true;
    }

    @Override
    public String toString(){
        return (new Integer(_value)).toString();
    }
}
