class Bool extends Type{
    private boolean _bool;
    Bool(boolean bool){
        _bool =bool;
    }

    Bool(boolean bool, boolean isTemp){
        _bool =bool;
        set_isTemp(isTemp);
    }

    Bool(Bool bool){
        _bool = bool._bool;
        set_isTemp(bool.get_isTemp());
    }
    @Override
    boolean isPrintable(){
        return true;
    }
    @Override
    public String toString(){
        if (_bool){
            return "True";
        }
        else{
            return "False";
        }
    }
    @Override
    String getType(){
        return "Bool";
    }
}
