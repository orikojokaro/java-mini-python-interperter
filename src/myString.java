class MyString extends  Sequence {
    private String _str;

    private boolean doesContainQoute(){
        for (int i = 0; i < _str.length(); i++){
            if (_str.charAt(i) == "'".charAt(0)){
                return true;
            }
        }
        return false;
    }
    MyString(String str) {
        _str = str;
    }

    MyString(MyString myStr){
        _str = myStr._str;
        set_isTemp(myStr.get_isTemp());
    }
    MyString(String str, boolean isTemp){
        _str = str;
        set_isTemp(isTemp);
    }
    @Override
    boolean isPrintable(){
        return true;
    }
    @Override
    public String toString(){
        StringBuilder strBuilder = new StringBuilder();
        if(doesContainQoute()){
            strBuilder.append('"');
            strBuilder.append(_str);
            strBuilder.append('"');
        }
        else{
            strBuilder.append("'");
            strBuilder.append(_str);
            strBuilder.append("'");
        }
        return strBuilder.toString();
    }
    @Override
    String getType(){
        return "MyString";
    }
}

